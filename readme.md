# Flyway + postgres

Flyway poc. Db: Postgres.

## Useful commands

* psql -d custom_db -U user
* \l 
	* Show tables
	* SELECT datname FROM pg_database;
* \c custom_db 
	* Change db 
* \dt
* Flyway migrations
	* select * from flyway_schema_history;

## Example of migrations 

|installed_rank | version | description 		| type 			| script 						| checksum 	  | installed_by | installed_on 			| execution_time | success  |
|1				| 001	  |	first migration.sql	| SQL			| V001__first_migration.sql.sql	| -1527846543 |	user		 | 2022-01-18 18:25:08.148	| 16			 | true		|


## Table

-- Drop table

-- DROP TABLE public.flyway_schema_history;

CREATE TABLE public.flyway_schema_history (
	installed_rank int4 NOT NULL,
	"version" varchar(50) NULL,
	description varchar(200) NOT NULL,
	"type" varchar(20) NOT NULL,
	script varchar(1000) NOT NULL,
	checksum int4 NULL,
	installed_by varchar(100) NOT NULL,
	installed_on timestamp NOT NULL DEFAULT now(),
	execution_time int4 NOT NULL,
	success bool NOT NULL,
	CONSTRAINT flyway_schema_history_pk PRIMARY KEY (installed_rank)
);
CREATE INDEX flyway_schema_history_s_idx ON public.flyway_schema_history USING btree (success);



## Referencias

* Flyway docs: https://flywaydb.org/documentation/
* Flyway docker: 
	* https://github.com/flyway/flyway-docker
	* https://hub.docker.com/r/flyway/flyway
* Migrations db:
	* https://flywaydb.org/documentation/concepts/migrations#naming
	* Name convention for migrations:
		* _Prefix_: V for versioned (configurable), U for undo (configurable) and R for repeatable migrations (configurable)
		* _Version_: Version with dots or underscores separate as many parts as you like (Not for repeatable migrations)
		* _Separator_: __ (two underscores) (configurable)
		* _Description_: Underscores or spaces separate the words
		* _Suffix_: .sql (configurable)
